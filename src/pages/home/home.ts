import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {DashPage} from "../dash/dash";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }
  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(DashPage, {
      item: item
    });
  }
}
