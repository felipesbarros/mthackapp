import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescontoPage } from './desconto';

@NgModule({
  declarations: [
    DescontoPage,
  ],
  imports: [
    IonicPageModule.forChild(DescontoPage),
  ],
})
export class DescontoPageModule {}
