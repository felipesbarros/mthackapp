import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganicPage } from './organic';

@NgModule({
  declarations: [
    OrganicPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganicPage),
  ],
})
export class OrganicPageModule {}
