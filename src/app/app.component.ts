import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DashPage } from '../pages/dash/dash';
import { PontosPage } from '../pages/pontos/pontos';
import { DescontoPage } from '../pages/desconto/desconto';
import { OrganicPage } from '../pages/organic/organic';
import { BarcodePage } from '../pages/barcode/barcode';
import { ProductDetailsPage } from '../pages/product-details/product-details';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'DashPage', component: DashPage },
      { title: 'PontosPage', component: PontosPage },
      { title: 'DescontoPage', component: DescontoPage },
      { title: 'OrganicPage', component: OrganicPage },
      { title: 'Barcodepage', component: BarcodePage },
      { title: 'ProductDetailsPage', component: ProductDetailsPage },

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
